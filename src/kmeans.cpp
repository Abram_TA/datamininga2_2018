#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <cmath>
using namespace std;

//global stuff
const int MAXITERATIONS = 0; //set to zero, if not used
const int RANDSIZE = 200;

//file input and output
void openFile(string fileName, fstream & ourFile);
void writeToFile(string fileName, const vector<vector<float>> & ourData, const int & numFeatures);
//working with vector
void populateVector(fstream & ourFile, vector<vector<float>> & ourData);
//k-means related
void getRandomCentroids(vector<vector<float>> & centroids, const int & k, const int & numFeatures);
bool shouldStop(const vector<vector<float>> & oldCentroids, const vector<vector<float>> & centroids, const int & iterations);
void setLabels(vector<vector<float>> & ourData, const vector<vector<float>> & centroids, const int & k, const int & numFeatures);
void setCentroids(vector<vector<float>> & centroids, const vector<vector<float>> & ourData, const int & k, const int & numFeatures);
double euclideanDistance(const vector<float> & dataPoint, const vector<float> & centroid, const int & numFeatures);

int main()
{
	//initial set of variables
	srand(time(NULL));
	const int k = 5;
	const string fileName = "../data/athlete_events_formatted.csv";
	const int numFeatures = 3;
	vector<vector<float>> ourData;
	fstream ourFile;

	//book keeping variables
	int iterations = 0;
	vector<vector<float>> oldCentroids;
	vector<vector<float>> centroids;

	//read our data and populate our dataSet
	openFile(fileName, ourFile);
	populateVector(ourFile, ourData);
	ourFile.close();

	//k-means algorithm
	cout << "k = " << k << " numFeatures = " << numFeatures << " iterations = " << MAXITERATIONS << endl << endl;
	bool firstRun = true;
	getRandomCentroids(oldCentroids, k, numFeatures);
	while (!shouldStop(oldCentroids, centroids, iterations))
	{
		//cout << "Inside kmeans loop" << endl;
		//some book keeping
		iterations += 1;
		cout << "Iteration " << iterations << ":" << endl;
		if (!firstRun) { oldCentroids = centroids; }
		else if (firstRun) { centroids = oldCentroids; }
		//assigning labels for each of the data points
		setLabels(ourData, centroids, k, numFeatures);
		//recalculating the centroids
		setCentroids(centroids, ourData, k, numFeatures);
		if (firstRun) { firstRun = false; }
	}

	//output our findings into .csv file:
	writeToFile("../data/kmeans_result.csv", ourData, numFeatures);

	cin.get();
}

void openFile(string fileName, fstream & ourFile)
{
	ourFile.open(fileName);
	if (ourFile.is_open())
	{
		cout << fileName << " is successfully opened!" << endl << endl;
	}
	else
	{
		cout << "Unable to open file!" << endl << endl;
	}
}
void writeToFile(string fileName, const vector<vector<float>> & ourData, const int & numFeatures)
{
	cout << "Calculations are done, putting them into .csv file..." << endl;
	fstream outFile;
	outFile.open(fileName, fstream::out | fstream::trunc);
	string row = "";
	for (int i = 0; i < ourData.size(); i++)
	{
		row.append(to_string(ourData[i][numFeatures]));
		row.append(",");
		for (int x = 0; x < numFeatures; x++)
		{
			if (x == numFeatures - 1)
			{
				row.append(to_string(ourData[i][x]));
			}
			else
			{
				row.append(to_string(ourData[i][x]));
				row.append(",");
			}
		}
		outFile << row;
		outFile << endl;
		row.clear();
	}
	outFile.close();
	cout << "Finished writing to the file!" << endl;
}
void populateVector(fstream & ourFile, vector<vector<float>> & ourData)
{
	cout << "The data is loading... Please wait..." << endl;
	if (ourFile.is_open())
	{
		string row;
		while (getline(ourFile, row))
		{
			vector<float> rowItems;
			string tempStr;
			for (int i = 0; i <= row.size(); i++)
			{
				if (row[i] == ',' || i == row.size())
				{
					rowItems.push_back(stoi(tempStr));
					tempStr = "";
				}
				else { tempStr += row[i]; }
			}
			ourData.push_back(rowItems);
			//for (auto& item : rowItems) { cout << item << " "; }
			//cout << endl;
		}
	}
	cout << "The data is all loaded up!" << endl << endl;
}
void getRandomCentroids(vector<vector<float>> & centroids, const int & k, const int & numFeatures)
{
	cout << "Our initial " << k << " centroids: " << endl;
	vector<float> centroidData;
	for (int y = 0; y < k; y++)
	{
		for (int x = 0; x < numFeatures; x++)
		{
			centroidData.push_back(rand() % RANDSIZE);
			cout << centroidData[x] << " ";
		}
		cout << endl;
		centroids.push_back(centroidData);
		centroidData.clear();
	}
	cout << endl;
}
bool shouldStop(const vector<vector<float>> & oldCentroids, const vector<vector<float>> & centroids, const int & iterations)
{
	//cout << "Inside shouldStop" << endl;
	if (iterations != 0 && iterations == MAXITERATIONS) { return true; } //max iterations was reached
	if (centroids.empty()) { return false; }
	else if (oldCentroids == centroids) { return true; } //centroids stoped changing, NEEDS FIXING
	return false;
}
void setLabels(vector<vector<float>> & ourData, const vector<vector<float>> & centroids, const int & k, const int & numFeatures)
{
	//for each select which centroid is closer (make it work for many centroids)
	//assign centroids as a fourth value, make sure not to screw up the ordering of centroids

	for (int i = 0; i < ourData.size(); i++)//go through every data point 
	{
		double min = euclideanDistance(ourData[i], centroids[0], numFeatures);
		int minIndex = 0;
		for (int x = 0; x < k; x++)
		{
			double distance = euclideanDistance(ourData[i], centroids[x], numFeatures);
			if (distance < min)
			{
				min = distance;
				minIndex = x;
			}
		}
		if (ourData[i].size() == numFeatures) { ourData[i].push_back(float(minIndex)); }
		else { ourData[i][numFeatures] = float(minIndex); }

		//for testing purposes
		/*for (int z = 0; z <= numFeatures; z++)
		{
			cout << z << ":" << ourData[i][z] << " ";
		}
		cout << endl;
		*/
	}
}
void setCentroids(vector<vector<float>> & centroids, const vector<vector<float>> & ourData, const int & k, const int & numFeatures)
{
	//each centroid is a geometric mean of the points with it's label
	//cout << "Inside setCentroids()" << endl;
	for (int c = 0; c < k; c++) //for each centroid
	{
		int numLabeled = 0;
		vector<float> featuresSum;
		for (int i = 0; i < numFeatures; i++) { featuresSum.push_back(0.f); }
		for (int i = 0; i < ourData.size(); i++)//go through our data points
		{
			if (ourData[i][numFeatures] == c) // if data point is labeled for that centroid
			{
				numLabeled++;
				for (int x = 0; x < numFeatures; x++) { featuresSum[x] += ourData[i][x]; }
			}
		}
		if (numLabeled == 0) {
			for (int r = 0; r < numFeatures; r++)
			{
				centroids[c][r] = rand() % RANDSIZE;
			}
		}
		cout << "New centroid #" << c << " : ";
		for (int s = 0; s < numFeatures; s++)
		{
			if (numLabeled != 0) { centroids[c][s] = featuresSum[s] / numLabeled; }
			cout << centroids[c][s] << " ";
		}
		cout << endl << "labeled " << numLabeled << " times" << endl;
	}
	cout << endl;
}
double euclideanDistance(const vector<float> & dataPoint, const vector<float> & centroid, const int & numFeatures)
{
	double underSquareRoot = 0;
	for (int i = 0; i < numFeatures; i++)
	{
		underSquareRoot += pow((dataPoint[i] - centroid[i]), 2.0);
	}
	return sqrt(underSquareRoot);
}