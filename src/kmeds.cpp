#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <cmath>
using namespace std;

//global variables
const int MAXITERATIONS = 200;
const int RANDSIZE = 200;
const int K = 5;
const int NUMFEATURES = 3;

//file input and output
void openFile(string fileName, fstream & ourFile);
void writeToFile(string fileName, const vector<vector<float>> & ourData);
//working with vector
void populateVector(fstream & ourFile, vector<vector<float>> & ourData);
void setMedoids(const vector<vector<float>> & ourData, vector<int> & medoids);
bool vecContains(const vector<int> & data, int & value);
//kmedoids related
double rectDistance(vector<float> & point, vector<float> & medoid);
void assignToMedoids(vector<vector<float>> & ourData, const vector<int> & medoids, vector<double> & clusterCost);
double differenceInSum(const vector<double> & oldClusterCost, const vector<double> & newClusterCost);
bool shouldStop(const int & iterations);
void tryDifferentMedoid(vector<vector<float>> & scrapData, vector<int> & scrapMedoids, vector<double> & newClusterCost);


int main()
{
	//initial stuff
	srand(time(NULL));
	const string fileName = "../data/athlete_events_formatted.csv";
	vector<vector<float>> ourData;
	vector<vector<float>> scrapData;
	vector<int> medoids;
	vector<int> scrapMedoids;
	vector<double> oldClusterCost;
	vector<double> clusterCost;
	fstream ourFile;

	//get the data in
	openFile(fileName, ourFile);
	populateVector(ourFile, ourData);
	ourFile.close();

	//some extra things before algorithm
	setMedoids(ourData, medoids);
	assignToMedoids(ourData, medoids, oldClusterCost);
	scrapData = ourData;
	scrapMedoids = medoids;

	//the main part of the algorithm:
	int iterations = 0;
	while (!shouldStop(iterations))
	{
		//book keeping
		iterations++;
		cout << "Iteration " << iterations << ":" << endl;

		//try a random point for a new medoid, check if the difference in sums is negative
		//if negative, get rid of old medoid
		tryDifferentMedoid(scrapData, scrapMedoids, clusterCost);
		//cout << "Passed tryDifferentMedoid" << endl;
		if (differenceInSum(oldClusterCost, clusterCost) < 0)
		{
			ourData = scrapData;
			medoids = scrapMedoids;
			oldClusterCost = clusterCost;
			cout << "New random medoid was good!" << endl;
		}
		else
		{
			clusterCost = oldClusterCost;
			cout << "Previous medoid was better..." << endl;
		}
		double sum = 0;
		for (auto& i : clusterCost) { sum += i; }

		/*cout << "Medoids:" << endl;
		for (auto& i : medoids)
		{
			for (auto& x : ourData[i])
			{
				cout << x << " ";
			}
			cout << endl;
		}*/
		cout << "Clustering sum: " << sum << endl;
		cout << endl;
	}

	//output the results to .csv
	writeToFile("../data/kmedoids_result.csv", ourData);
	cin.get();
}

void openFile(string fileName, fstream & ourFile)
{
	ourFile.open(fileName);
	if (ourFile.is_open())
	{
		cout << fileName << " is successfully opened!" << endl << endl;
	}
	else
	{
		cout << "Unable to open file!" << endl << endl;
	}
}
void writeToFile(string fileName, const vector<vector<float>> & ourData)
{
	cout << "Calculations are done, putting them into .csv file..." << endl;
	fstream outFile;
	outFile.open(fileName, fstream::out | fstream::trunc);
	string row = "";
	for (int i = 0; i < ourData.size(); i++)
	{
		row.append(to_string(ourData[i][NUMFEATURES]));
		row.append(",");
		for (int x = 0; x < NUMFEATURES; x++)
		{
			if (x == NUMFEATURES - 1)
			{
				row.append(to_string(ourData[i][x]));
			}
			else
			{
				row.append(to_string(ourData[i][x]));
				row.append(",");
			}
		}
		outFile << row;
		outFile << endl;
		row.clear();
	}
	outFile.close();
	cout << "Finished writing to the file!" << endl;
}
void populateVector(fstream & ourFile, vector<vector<float>> & ourData)
{
	cout << "The data is loading... Please wait..." << endl;
	if (ourFile.is_open())
	{
		string row;
		while (getline(ourFile, row))
		{
			vector<float> rowItems;
			string tempStr;
			for (int i = 0; i <= row.size(); i++)
			{
				if (row[i] == ',' || i == row.size())
				{
					rowItems.push_back(stoi(tempStr));
					tempStr = "";
				}
				else { tempStr += row[i]; }
			}
			ourData.push_back(rowItems);
			//for (auto& item : rowItems) { cout << item << " "; }
			//cout << endl;
		}
	}
	cout << "The data is all loaded up!" << endl << endl;
}
void setMedoids(const vector<vector<float>> & ourData, vector<int> & medoids)
{
	//initial medoid selection
	if (!medoids.empty()) { return; }
	for (int i = 0; i < K; i++)
	{
		medoids.push_back(rand() % ourData.size());
	}
}
bool vecContains(const vector<int> & data, int & value)
{
	bool contains = false;
	for (int i = 0; i < data.size(); i++)
	{
		if (data[i] == value)
		{
			contains = true;
			break;
		}
	}
	return contains;
}
double rectDistance(vector<float> & point1, vector<float> & point2)
{
	double rectiliniarDistance = 0;
	for (int i = 0; i < NUMFEATURES; i++)
	{
		rectiliniarDistance += abs(point1[i] - point2[i]);
	}
	return rectiliniarDistance;
}
void assignToMedoids(vector<vector<float>> & ourData, const vector<int> & medoids, vector<double> & clusterCost)
{
	//reset the clusterCosts
	for (int i = 0; i < medoids.size(); i++)
	{
		if (clusterCost.size() != medoids.size()) { clusterCost.push_back(0); }
		else { clusterCost[i] = 0; }
	}

	//assign points to medoids
	for (int i = 0; i < ourData.size(); i++)
	{
		if (!vecContains(medoids, i))
		{
			double min = rectDistance(ourData[i], ourData[medoids[0]]);
			int minIndex = 0;
			for (int x = 1; x < K; x++)
			{
				double distance = rectDistance(ourData[i], ourData[medoids[x]]);
				if (distance < min)
				{
					min = distance;
					minIndex = x;
				}
			}
			clusterCost[minIndex] += min;
			if (ourData[i].size() == NUMFEATURES)
			{
				ourData[i].push_back(medoids[minIndex]);
			}
			else
			{
				ourData[i][NUMFEATURES] = medoids[minIndex];
			}
		}
		else
		{
			//medoids are attached to themselves
			ourData[i].push_back(i);
		}
	}
}
double differenceInSum(const vector<double> & oldClusterCost, const vector<double> & newClusterCost)
{
	double sum1 = 0, sum2 = 0;
	//cout << "Old size:" << oldClusterCost.size() << " New size: " << newClusterCost.size() << endl;
	for (int i = 0; i < oldClusterCost.size(); i++)
	{
		//cout << oldClusterCost[i] << " + " << newClusterCost[i] << endl;
		sum1 += oldClusterCost[i];
		sum2 += newClusterCost[i];
	}
	//cout << endl;
	return double(sum2 - sum1);
}
bool shouldStop(const int & iterations)
{
	if (iterations == MAXITERATIONS) { return true; }
	return false;
}
void tryDifferentMedoid(vector<vector<float>> & scrapData, vector<int> & scrapMedoids, vector<double> & newClusterCost)
{
	//find a random point that is not a medoid
	int index = rand() % scrapData.size();
	while (vecContains(scrapMedoids, index))
	{
		index = rand() % scrapData.size();
	}
	//cout << "Random index: " << index << endl;

	//swap the medoid this point belongs to with the randomly found point
	for (int i = 0; i < scrapMedoids.size(); i++)
	{
		//cout << scrapMedoids[i] << " ";
		if (scrapData[index][NUMFEATURES] == scrapMedoids[i])
		{
			scrapMedoids[i] = index;
		}
		//cout << scrapMedoids[i] << endl;

	}
	//cout << endl;

	//reset the clusterCosts
	for (int i = 0; i < scrapMedoids.size(); i++)
	{
		if (newClusterCost.size() != scrapMedoids.size()) { newClusterCost.push_back(0); }
		else { newClusterCost[i] = 0; }
	}

	//determine the closest medoids + calculate the distances + new sum
	//assign points to medoids
	for (int i = 0; i < scrapData.size(); i++)
	{
		if (!vecContains(scrapMedoids, i))
		{
			double min = rectDistance(scrapData[i], scrapData[scrapMedoids[0]]);
			int minIndex = 0;
			for (int x = 1; x < K; x++)
			{
				double distance = rectDistance(scrapData[i], scrapData[scrapMedoids[x]]);
				if (distance < min)
				{
					min = distance;
					minIndex = x;
				}
			}
			newClusterCost[minIndex] += min;
			if (scrapData[i].size() == NUMFEATURES)
			{
				scrapData[i].push_back(scrapMedoids[minIndex]);
			}
			else
			{
				scrapData[i][NUMFEATURES] = scrapMedoids[minIndex];
			}
		}
	}
}