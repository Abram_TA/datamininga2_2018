// Data Mining and Machine Learning
// Fall 2018
// Andrew Leonov

// Agglomerative Hierarchical Clustering / Bottom Up Clustering
// Implements Centroidal Linkage

// Each point has a unique ID, which is used to generate cluster ID 
// when it's added to a cluster. The global cluster will have all of the points' IDs

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <math.h>
#include <sstream>

using namespace std;

// Structure to represent a point (i.e a row from the dataset)
struct point {
	string clustID;
	double age, height, weight;
	bool isInCluster;
};

struct node {
	point info;
	node *left;
	node *right;
};


// Functions to read from file and get data into memory
void openFile(string fileName, ifstream & file);
void populateVector(ifstream & ourFile, vector<point> & ourData);

// Calculates Euclidean Distance
double calcDistance(double x1, double x2, double y1, double y2, double z1, double z2);

// Function that merges clusters
string mergeID(node *n);

// Function managing the root parent when merging
node merge(node n1, node n2);

// Function to display the structure of the clusters
void printCluster(node root);

int main()
{
	ifstream file;
	string fileName = "../data/athlete_events_formatted.csv"; // file to open

	vector<point> data; // vector to store data read from file
	vector<node> clusters; // vector to store actual nodes

	openFile(fileName, file);
	populateVector(file, data); // read data from file into vector

	// this will convert data read from file
	// and create 'points' out of them
	for (int i = 0; i < data.size(); i++) {
		node point;
		point.info = data[i]; // create nodes for every point
		point.left = point.right = NULL;
		point.info.clustID = to_string(i);

		clusters.push_back(point);
	}

	cout << "\nWorking...\n";

	// initial distance that determines how far away
	// clusters have to be in order to be in a cluster
	int distance = 10;
	// ***************

	while (clusters.size() > 1) {
		for (int i = 0; i < clusters.size(); i++) {
			if (!clusters.at(i).info.isInCluster) {
				for (int x = 0; x < clusters.size(); x++) {
					// loop through the rest and compare distances
					if (x == i || clusters.at(i).info.isInCluster || clusters.at(x).info.isInCluster) {
						continue; // skip element to avoid comparison to itself
					}

					double dist = calcDistance(clusters.at(i).info.height, clusters.at(x).info.height, clusters.at(i).info.weight, clusters.at(x).info.weight, clusters.at(i).info.age, clusters.at(x).info.age);

					if (dist < distance) {
						// mark both points to show they're in a cluster
						clusters.at(i).info.isInCluster = true;
						clusters.at(x).info.isInCluster = true;

						node clust = merge(clusters.at(i), clusters.at(x));

						clusters.erase(clusters.begin() + i);
						if (x < i) {
							clusters.erase(clusters.begin() + x);
						}
						else {
							clusters.erase(clusters.begin() + (x - 1));
						}

						clusters.push_back(clust);

						x = 0; // reset counter
					}
				}
			}
		}
		distance += 5;
	}

	printCluster(clusters.at(0));

	cin.get();
	return 0;
}

void openFile(string fileName, ifstream & file) {
	cout << "Opening file...\n";
	file.open(fileName);
	if (file.is_open()) {
		cout << "File opened successfully.\n\n";
	}
	else {
		cout << "Unable to open file.\n\n";
	}
}

void populateVector(ifstream & file, vector<point> & data)
{
	if (file.is_open())
	{
		string row;

		cout << "Reading data from file...\n\n";
		while (getline(file, row))
		{
			int commaFound = 0;
			point rowItems;
			string tempStr;

			rowItems.isInCluster = false;

			for (int i = 0; i < row.size(); i++)
			{
				if (row[i] == ',')
				{
					if (commaFound == 0) // first comma
					{
						rowItems.age = atoi(tempStr.c_str()); // cast string to int and store it
						tempStr = ""; // reset the string
						commaFound = 1;
					}
					else if (commaFound == 1) // second comma
					{
						rowItems.height = atoi(tempStr.c_str()); // cast string to int and store it
						tempStr = "";
					}
				}
				else {
					tempStr += row[i];
				}
			}
			rowItems.weight = atoi(tempStr.c_str()); // cast string to int and store it

			data.push_back(rowItems); // save new point into vector
		}
	}
}

double calcDistance(double x1, double x2, double y1, double y2, double z1, double z2) {
	return sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2) + pow(z2 - z1, 2));
}

string mergeID(node *n) {
	if (n != NULL && n->info.clustID == "") {
		n->info.clustID = n->info.clustID + " " + mergeID(n->left) + " " + mergeID(n->right);
	}
	return n->info.clustID;
}

node merge(node n1, node n2) {
	node* newClust = new node;
	node* node1 = new node(n1);
	node* node2 = new node(n2);

	newClust->info.isInCluster = false;
	newClust->left = node1;
	newClust->right = node2;

	// merge cluster IDs to show what's included
	mergeID(newClust);

	// find the center of the new cluster
	newClust->info.age = (node1->info.age + node2->info.age) / 2;
	newClust->info.height = (node1->info.height + node2->info.height) / 2;
	newClust->info.weight = (node1->info.weight + node2->info.weight) / 2;

	return *newClust;
}

void printCluster(node root) {
	cout << "\n\nIDs of clusters inside: " << root.info.clustID;

	// recursively call on the clusters inside
	if (root.left != NULL) {
		printCluster(*root.left);
	}
	if (root.right != NULL) {
		printCluster(*root.right);
	}

	if (root.left == NULL && root.right == NULL) {
		// if this is an actual point, not a cluster - show its details
		cout << "\nPoint ID: " << root.info.clustID << "\nAge: " << root.info.age << "\nHeight: " << root.info.height << "\nWeight: " << root.info.weight;
	}
}