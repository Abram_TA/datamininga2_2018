install.packages("ggplot2")
library("ggplot2")
setwd("C:/Users/bowga/Documents/GitHub/datamininga2_2018/data")
kmeansdata <- read.csv("kmeans_resultR.csv")
ggplot(kmeansdata, aes(Weight, Height, color = Cluster)) + geom_point()
kmedoidsdata <- read.csv("kmedoids_resultR.csv")
ggplot(kmedoidsdata, aes(Weight, Height, color = Cluster)) + geom_point()