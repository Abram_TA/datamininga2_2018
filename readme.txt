Data Mining/Machine Learning
Fall 2018

Team Members:
- Andrew Leonov
- Timothy Abramov
- Benjamin Weisman


All source code files are located in /src
All data files, output files, and diagrams are located in /data

==============================
Compiling and Running Code
==============================
- download the source code
- sudo apt-get install g++ (if you don't have installed yet)
- browse to the /src folder of the project
- compile the code with 
	g++ -std=c++11 alg_name.cpp -o alg_name
- run the compiled executable with
	./alg_name


================================
Running R Code for Visualization
================================
- open the .r file located in /src using R Studio